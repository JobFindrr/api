package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/JobFindrr/api/entities"
	"gitlab.com/JobFindrr/api/helpers"
)

func RootHandler(rw http.ResponseWriter, r *http.Request) {
	rw.Write([]byte("Hello world!"))
}

func LoginHandler(rw http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusBadRequest)

		return
	}

	var userData *entities.User

	err = json.Unmarshal(data, &userData)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusExpectationFailed)
		rw.Write([]byte("Incorrect login information"))

		return
	}

	user, err := userData.FindByEmail()

	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte("Incorrect login information"))

		return
	}

	err = user.CheckPassword(userData.Password)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte("Incorrect login information"))

		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte("Login successful"))
}

func RegisterHandler(rw http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusBadRequest)

		return
	}

	var userData *entities.User

	err = json.Unmarshal(data, &userData)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusExpectationFailed)
		rw.Write([]byte("Invalid data format"))

		return
	}

	userData.Password, err = helpers.HashPassword(userData.Password)

	if err != nil {
		log.Println(err.Error())

		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte("Error when creating account"))

		return
	}

	err = userData.Create()

	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte("User already exists"))

		return
	}

	rw.WriteHeader(http.StatusAccepted)
	rw.Write([]byte("Account creation successful!"))
}
