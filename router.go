package main

import (
	"net/http"

	"gitlab.com/JobFindrr/api/handlers"

	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter()

	// Force redirect routes with trailing slash
	router.StrictSlash(true)

	root := router.PathPrefix("/").Subrouter()
	root.HandleFunc("/", handlers.RootHandler).Methods(http.MethodGet)
	root.HandleFunc("/login", handlers.LoginHandler).Methods(http.MethodPost)
	root.HandleFunc("/register", handlers.RegisterHandler).Methods(http.MethodPost)

	users := router.PathPrefix("/users").Subrouter()
	users.HandleFunc("/", handlers.UsersRootHandler).Methods(http.MethodGet)

	return router
}
