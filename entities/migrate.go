package entities

import "gitlab.com/JobFindrr/api/db"

func Migrate() {
	db := db.NewConnection()

	db.AutoMigrate(&User{})
}
