package entities

import (
	"log"

	"gitlab.com/JobFindrr/api/db"
	"gitlab.com/JobFindrr/api/helpers"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username string `json:"username" gorm:"string;unique"`
	Password string `json:"password" gorm:"string;not null"`
	Email    string `json:"email" gorm:"string;unique;not null"`
}

func (user *User) Create() error {
	db := db.NewConnection()

	result := db.Create(user)

	if result.Error != nil {
		log.Println(result.Error.Error())

		return result.Error
	}

	return nil
}

func (user *User) CheckPassword(password string) error {
	return helpers.ComparePasswords(user.Password, password)
}

func (user *User) FindByEmail() (*User, error) {
	var dbUser *User

	db := db.NewConnection()

	result := db.First(&dbUser, "email = ?", user.Email)

	if result.Error != nil {
		log.Println(result.Error.Error())

		return nil, result.Error
	}

	return dbUser, nil
}
