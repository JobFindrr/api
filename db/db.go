package db

import (
	"log"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var datetimePrecision int = 2

var MysqlConfig *mysql.Config = &mysql.Config{
	DSN:                       getDbConnDataFromEnv() + "?charset=utf8&parseTime=True&loc=Local", // data source name, refer https://github.com/go-sql-driver/mysql#dsn-data-source-name
	DefaultStringSize:         256,                                                               // add default size for string fields, by default, will use db type `longtext` for fields without size, not a primary key, no index defined and don't have default values
	DisableDatetimePrecision:  true,                                                              // disable datetime precision support, which not supported before MySQL 5.6
	DefaultDatetimePrecision:  &datetimePrecision,                                                // default datetime precision
	DontSupportRenameIndex:    true,                                                              // drop & create index when rename index, rename index not supported before MySQL 5.7, MariaDB
	DontSupportRenameColumn:   true,                                                              // use change when rename column, rename rename not supported before MySQL 8, MariaDB
	SkipInitializeWithVersion: false,                                                             // smart configure based on used version
}

var GormConfig *gorm.Config = &gorm.Config{}

func NewConnection() *gorm.DB {
	mysqlConnection := mysql.New(*MysqlConfig)

	db, err := gorm.Open(mysqlConnection, GormConfig)

	if err != nil {
		log.Println(err.Error())

		panic("Could not connect to the database")
	}

	return db
}

func getDbConnDataFromEnv() string {
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	db := os.Getenv("DB_DATABASE")
	dbProtocol := os.Getenv("DB_PROTOCOL")

	if len(dbUser) == 0 || len(dbPass) == 0 || len(db) == 0 {
		log.Fatal("Required DB connection information is missing!")

		return ""
	}

	if len(dbHost) == 0 {
		dbHost = "localhost"
	}

	if len(dbPort) == 0 {
		dbPort = "3306"
	}

	if len(dbProtocol) == 0 {
		dbProtocol = "tcp"
	}

	return dbUser + ":" + dbPass + "@" + dbProtocol + "(" + dbHost + ")/" + db
}
