package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/JobFindrr/api/entities"
)

var serverWait time.Duration

var defaultPort string = "3003"

func parseFlags() {
	flag.DurationVar(&serverWait, "graceful-shutdown", time.Second*15, "the duration for which the server will gracefully wait for existing connections to finish")

	flag.Parse()
}

func runServer(srv *http.Server) {
	log.Println("Server started and listening on port", getServerPort())

	if err := srv.ListenAndServe(); err != nil {
		log.Println(err)
	}
}

func getServerPort() string {
	envPort := os.Getenv("PORT")

	if len(envPort) == 0 {
		return defaultPort
	}

	return envPort
}

func RunServer() {
	parseFlags()

	server := http.Server{
		Addr:         ":" + getServerPort(),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      NewRouter(),
	}

	go runServer(&server)

	entities.Migrate()

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGABRT)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), serverWait)

	defer cancel()

	server.Shutdown(ctx)

	log.Println("Shutting server down")

	os.Exit(0)
}
